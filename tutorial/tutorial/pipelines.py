# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
import re
from itemadapter import ItemAdapter
from scrapy.exceptions import DropItem


class PricePipeline:

    def process_item(self, item, spider):
        adapter = ItemAdapter(item)
        if adapter.get('price'):
            my_price = re.findall(
                '\d+', adapter.get('price').replace(',', ''))[0]
            adapter['price'] = [int(s)
                                for s in str.split(my_price) if s.isdigit()][0]
            return item
        else:
            raise DropItem(f"Missing price in {item}")


class MileagePipeline:

    def process_item(self, item, spider):
        adapter = ItemAdapter(item)
        if adapter.get('mileage'):
            mileage = re.findall(
                '\d+', adapter.get('mileage').replace(',', ''))[0]
            adapter['mileage'] = [int(s)
                                  for s in str.split(mileage) if s.isdigit()][0]
            return item
        else:
            raise DropItem(f"Missing price in {item}")


class ImagesPipeline:

    def process_item(self, item, spider):
        adapter = ItemAdapter(item)

        if adapter.get('images'):
            gallery = []
            for image in adapter.get('images'):
                gallery.append({"image": image})
            adapter['images'] = gallery
            return item
        else:
            raise DropItem(f"Missing price in {item}")
