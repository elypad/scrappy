import scrapy


# item class included here
class CarItem(scrapy.Item):
    # define the fields for your item here like:
    carLink = scrapy.Field()
    price = scrapy.Field()
    carTitle = scrapy.Field()
    extras = scrapy.Field()
    images = scrapy.Field()
    description = scrapy.Field()
    model = scrapy.Field()
    body = scrapy.Field()
    model = scrapy.Field()
    year = scrapy.Field()
    mileage = scrapy.Field()
    transmission = scrapy.Field()
    warranty = scrapy.Field()
    interiorColor = scrapy.Field()
    color = scrapy.Field()
    cylinderCount = scrapy.Field()
    fuelType = scrapy.Field()
    doors = scrapy.Field()


class QuotesSpider(scrapy.Spider):

    name = "quotes"
    allowed_domains = ["motorgy.com"]
    BASE_URL = 'https://www.motorgy.com'
    start_urls = [
        'https://www.motorgy.com/en/used-cars',
        'https://www.motorgy.com/en/used-cars?pn=2',
    ]

    # FUNCTION TO FOLLOW EACH CAR LINK AND CRAP MORE CAR DETAILS
    def parse(self, response):

        for quote in response.css('div.forRelative'):
            absolute_url = self.BASE_URL + \
                quote.css('div.text-center > a::attr(href)').get()
            yield scrapy.Request(absolute_url, callback=self.parse_more_details)
            print("OPENING NEXT LINK: ", quote.css(
                'div.text-center > a::attr(href)').get())

        # NAVIGATE TO NEXT PAGE AT A TIME - new methord to follow links
        for next_page in response.css('div.justify-content-end > a.activeLink::attr(href)'):
            yield response.follow(next_page, callback=self.parse)
            print("OPENING NEXT PAGE: ", next_page)

    # FUNCTION TO FOLLOW CAR LINK AND CRAP MORE CAR DETAILS

    def parse_more_details(self, response):

        item = CarItem()

        item["carLink"] = response.url

        my_price = response.css('div.fs-25::text').get()
        item["price"] = my_price

        item["carTitle"] = response.css(
            'div.car-details-name > h2::text').get()

        my_description = response.css('p.fs-18::text').get()
        item["description"] = my_description.replace("\n", "")

        my_extras = response.css(
            'ul.features-car-details-page1 > li > span::text').getall()

        item["extras"] = [sub.replace('-', '') for sub in my_extras]

        item["images"] = response.css(
            'div#car-details-slider-img-wrap > a::attr(href)').getall()

        specs = response.css('table.tcd-single > tr > td::text').getall()

        item["body"] = specs[1]

        item["model"] = specs[3]

        item["year"] = specs[5]

        item["mileage"] = specs[7]

        item["transmission"] = specs[9]

        item["warranty"] = specs[11]

        item["interiorColor"] = specs[13]

        item["color"] = specs[15]

        item["cylinderCount"] = specs[17]

        item["fuelType"] = specs[19]

        item["doors"] = specs[21]

        return item
